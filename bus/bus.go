package bus

import "bitbucket.org/nildev/lib/bus/command"

type (
	// Interface type
	Interface interface {
		Execute(command.Command) error
		Get(command.ID) (command.Command, error)
	}

	// Bus type
	Bus struct {
		cmdReader    command.Reader
		cmdPersister command.Writer
	}
)

// Get command
func (b *Bus) Get(cmdID command.ID) (command.Command, error) {
	return b.cmdReader.ReadByID(cmdID)
}
