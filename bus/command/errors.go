package command

// ErrorData structure to store error code and description
type ErrorData struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// Error codes for ErrorData
const (
	ErrorCodeRequestJSONParseError = 901
	ErrorCodeDomainValidationError = 1001
	ErrorCodeDatabaseSaveError     = 801
)

// RequestJSONParseErrorData returns new resource with parse error Code
func RequestJSONParseErrorData(details error) *ErrorData {
	return &ErrorData{
		Code:    ErrorCodeRequestJSONParseError,
		Message: "Request invalid. JSON parse error: " + details.Error(),
	}
}

// DomainValidationErrorData returns new resource with domain validation error Code
func DomainValidationErrorData(details error) *ErrorData {
	return &ErrorData{
		Code:    ErrorCodeDomainValidationError,
		Message: "Domain validation failed: " + details.Error(),
	}
}

// DatabaseSaveErrorData returns new resource with database error message
func DatabaseSaveErrorData(details error) *ErrorData {
	return &ErrorData{
		Code:    ErrorCodeDatabaseSaveError,
		Message: "Failed to save to database. Try retrying with new id: " + details.Error(),
	}
}
