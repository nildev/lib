package command

import "sync"

type (
	// CommandsStorage type
	CommandsStorage struct {
		sync.RWMutex
		cmds map[ID]Command
	}

	// InMemoryCommandReadWriteRemover type
	InMemoryCommandReadWriteRemover struct {
		Cmds     *CommandsStorage
		ReadCmds *CommandsStorage
	}
)

// NewInMemoryCommandReadWriteRemover type
func NewInMemoryCommandReadWriteRemover() *InMemoryCommandReadWriteRemover {
	return &InMemoryCommandReadWriteRemover{
		Cmds: &CommandsStorage{
			cmds: make(map[ID]Command),
		},
		ReadCmds: &CommandsStorage{
			cmds: make(map[ID]Command),
		},
	}
}

// Read command
func (imcrw *InMemoryCommandReadWriteRemover) Read() (Command, error) {
	imcrw.Cmds.Lock()
	imcrw.ReadCmds.Lock()
	var rez Command
	var k ID
	var v Command
	for k, v = range imcrw.Cmds.cmds {
		rez = v
		break
	}
	delete(imcrw.Cmds.cmds, k)
	imcrw.Cmds.Unlock()
	imcrw.ReadCmds.Unlock()
	return rez, nil
}

// ReadByID command
func (imcrw *InMemoryCommandReadWriteRemover) ReadByID(cmdID ID) (Command, error) {
	imcrw.Cmds.RLock()
	rez := imcrw.Cmds.cmds[cmdID]
	imcrw.Cmds.RUnlock()

	return rez, nil
}

// Write command
func (imcrw *InMemoryCommandReadWriteRemover) Write(cmd Command) error {
	imcrw.Cmds.Lock()
	imcrw.ReadCmds.Lock()
	imcrw.Cmds.cmds[cmd.GetID()] = cmd
	imcrw.ReadCmds.cmds[cmd.GetID()] = cmd
	imcrw.Cmds.Unlock()
	imcrw.ReadCmds.Unlock()
	return nil
}

// Update command
func (imcrw *InMemoryCommandReadWriteRemover) Update(cmd Command) error {
	imcrw.ReadCmds.Lock()
	imcrw.ReadCmds.cmds[cmd.GetID()] = cmd
	imcrw.ReadCmds.Unlock()
	return nil
}

// Remove command
func (imcrw *InMemoryCommandReadWriteRemover) Remove(cmd Command) error {
	imcrw.ReadCmds.Lock()
	delete(imcrw.Cmds.cmds, cmd.GetID())
	delete(imcrw.ReadCmds.cmds, cmd.GetID())
	imcrw.ReadCmds.Unlock()
	return nil
}
