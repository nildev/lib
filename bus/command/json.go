package command

import (
	"encoding/json"

	"bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"
)

// NewJSONUnserializer Creates Unserializer
func NewJSONUnserializer(commandTypes Builder) Unserializer {
	return &JSONUnserializer{commandTypes: commandTypes}
}

// JSONUnserializer is internal configuration structure for unserializer
type JSONUnserializer struct {
	commandTypes Builder
}

// Unserialize takes JSON input and creates command from it
func (s *JSONUnserializer) Unserialize(raw []byte) (Command, error) {
	var err error
	commandRaw := make(map[string]interface{})

	if err = json.Unmarshal(raw, &commandRaw); err != nil {
		return nil, errors.Trace(err)
	}

	if commandName, ok := commandRaw["CommandName"].(string); ok {
		if cmd := s.commandTypes.Build(commandName); cmd != nil {

			if err = json.Unmarshal(raw, &cmd); err == nil {
				return cmd, nil
			}

			return nil, errors.Trace(errors.Errorf("Unable to find Command %s", string(commandName)))
		}
		return nil, errors.Trace(err)
	}

	return nil, errors.Trace(errors.Errorf("Unable to find key CommandName, input: %s", string(raw)))
}

// JSONSerializer struct for serializer
type JSONSerializer struct{}

// NewJSONSerializer Creates instance of serializer
func NewJSONSerializer() Serializer {
	return &JSONSerializer{}
}

// Serialize takes command and returns JSON serialized command
func (*JSONSerializer) Serialize(command Command) ([]byte, error) {
	return json.Marshal(command)
}

// JSONSerializerUnserializer internal structure for both operations
type JSONSerializerUnserializer struct {
	Serializer
	Unserializer
}

// NewJSONSerializerUnserializer Create serializer capable of serializing and unserializing
func NewJSONSerializerUnserializer(commandTypes Builder) SerializeUnserializer {
	return &JSONSerializerUnserializer{
		NewJSONSerializer(),
		NewJSONUnserializer(commandTypes),
	}
}
