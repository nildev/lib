package command

import (
	"time"
)

type (
	// Status type
	Status string

	// ID of command
	ID string

	// Queue of commands
	Queue chan Command

	// Resource type
	Resource struct {
		Type string                 `json:"type"`
		Attr map[string]interface{} `json:"attr"`
	}

	// ResourcesCollection type
	ResourcesCollection []*Resource

	// Command type
	Command interface {
		GetID() ID
		GetName() string
		GetStatus() Status
		GetCreatedAt() time.Time
		GetUpdatedAt() time.Time
		GetResults() ResourcesCollection
		GetError() *ErrorData
		Execute() error
		Validate() error
	}

	// Reader for commands
	Reader interface {
		ReadByID(ID) (Command, error)
	}

	// Subscriber for commands
	Subscriber interface {
		Read() (Command, error)
	}

	// Writer for commands
	Writer interface {
		Publisher
		Update(Command) error
	}

	// Publisher for commands
	Publisher interface {
		Write(Command) error
	}

	// Remover for commands
	Remover interface {
		Remove(Command) error
	}

	// Serializer interface for command serialization
	Serializer interface {
		Serialize(Command) ([]byte, error)
	}

	// Unserializer interface for command unserialization
	Unserializer interface {
		Unserialize([]byte) (Command, error)
	}

	// SerializeUnserializer interface
	SerializeUnserializer interface {
		Serializer
		Unserializer
	}

	// Builder interface to build command by given name
	Builder interface {
		Build(string) Command
	}

	// BaseCommand type
	BaseCommand struct {
		CmdID        ID                  `json:"CommandID"`
		CmdName      string              `json:"CommandName"`
		CmdStatus    Status              `json:"CommandStatus"`
		CmdCreatedAt time.Time           `json:"CommandCreatedAt"`
		CmdUpdatedAt time.Time           `json:"CommandUpdatedAt"`
		CmdResults   ResourcesCollection `json:"CommandResults"`
		CommandError *ErrorData          `json:"CommandError,omitempty"`
	}
)

// Constants
const (
	StatusPending    Status = "pending"
	StatusInProgress Status = "inProgress"
	StatusDone       Status = "done"
	StatusFailed     Status = "failed"
)

// NewBaseCommand command implementation
func NewBaseCommand(id ID, name string) BaseCommand {
	return BaseCommand{
		CmdID:      id,
		CmdName:    name,
		CmdStatus:  StatusPending,
		CmdResults: ResourcesCollection{},
	}
}

// GetID of command
func (bc BaseCommand) GetID() ID {
	return bc.CmdID
}

// GetName of command
func (bc BaseCommand) GetName() string {
	return bc.CmdName
}

// GetStatus of command
func (bc BaseCommand) GetStatus() Status {
	return bc.CmdStatus
}

// GetCreatedAt of command
func (bc BaseCommand) GetCreatedAt() time.Time {
	return bc.CmdCreatedAt
}

// GetUpdatedAt of command
func (bc BaseCommand) GetUpdatedAt() time.Time {
	return bc.CmdUpdatedAt
}

// GetError of command
func (bc BaseCommand) GetError() *ErrorData {
	return bc.CommandError
}

// Execute command
func (bc BaseCommand) Execute() error {
	return nil
}

// Validate command
func (bc BaseCommand) Validate() error {
	return nil
}

// GetResults from command
func (bc BaseCommand) GetResults() ResourcesCollection {
	return bc.CmdResults
}
