package bus

import (
	"bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"
	"bitbucket.org/nildev/lib/bus/command"
)

type (
	// Sync type
	Sync struct {
		*Bus
	}
)

// NewSyncBus returns Sync instance
func NewSyncBus(cmdReader command.Reader, cmdPersister command.Writer) *Sync {
	return &Sync{
		&Bus{
			cmdReader,
			cmdPersister,
		},
	}
}

// Execute command
func (s *Sync) Execute(cmd command.Command) error {
	if err := s.cmdPersister.Write(cmd); err != nil {
		return errors.Trace(err)
	}

	// We execute only commands that are marked in progress
	// if command has failed before - write it to storage but do not execute
	var errCmd error
	if cmd.GetStatus() == command.StatusInProgress {
		errCmd = cmd.Execute()
	}

	// update storage
	if errSave := s.cmdPersister.Update(cmd); errSave != nil {
		return errors.Trace(errSave)
	}

	// return error that came from execution
	if errCmd != nil {
		return errors.Trace(errCmd)
	}

	return nil
}
