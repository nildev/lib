package storage

import (
	"bitbucket.org/nildev/lib/eventstore/event"
)

// NewInMemoryStore creates a new instance of event store which uses in-memory
// array to store events. Useful for testing only, should not be used in
// production.
func NewInMemoryStore(events ...event.Envelope) EventStore {
	es := &inMemory{
		serializer: event.NewJSONSerializerUnserializer(
			event.NewJSONSerializer(),
			event.NewJSONUnserializer(),
		),
	}

	es.writeSlice(events)

	return es
}

// NewInMemoryStoreSerializer allows to provide custom serializer
func NewInMemoryStoreSerializer(srz event.SerializeUnserializer, events ...event.Envelope) EventStore {
	es := &inMemory{
		serializer: srz,
	}

	es.writeSlice(events)

	return es
}

// Private stuff
// In-memory store implementation

type inMemory struct {
	events     []string
	serializer event.SerializeUnserializer
}

// Process implements pipeline interface
func (es *inMemory) Process(in chan event.Envelope, errCh chan error) chan event.Envelope {
	buffer := make(chan event.Envelope, 100)
	out := make(chan event.Envelope)
	// We write all events first and only then we start writing to outbound channel
	go func() {
		for evt := range in {
			w := make(chan event.Envelope, 1)
			w <- evt
			close(w)

			err := es.Write(w)
			if err != nil {
				errCh <- err
				// we stop processing events
				break
			}
			// pass forward
			buffer <- evt
		}
		close(buffer)
		for evt := range buffer {
			out <- evt
		}

		close(out)
	}()

	return out
}

func (es *inMemory) ReadByRequestID(requestID string, ch chan<- event.Envelope) error {
	defer close(ch)

	for _, evt := range es.events {
		evt, err := es.serializer.Unserialize(evt)
		if err != nil {
			return err
		}

		if evt.RequestID() == requestID {
			ch <- evt
		}
	}

	return nil
}

func (es *inMemory) Read(aggregateID string, ch chan<- event.Envelope) error {
	defer close(ch)

	for _, evt := range es.events {
		evt, err := es.serializer.Unserialize(evt)
		if err != nil {
			return err
		}

		if evt.AggregateID() == aggregateID {
			ch <- evt
		}
	}

	return nil
}

func (es *inMemory) ReadAll(ch chan<- event.Envelope) error {
	defer close(ch)

	for _, evt := range es.events {
		event, err := es.serializer.Unserialize(evt)
		if err != nil {
			return err
		}

		ch <- event
	}

	return nil
}

func (es *inMemory) Write(ch <-chan event.Envelope) error {
	for evt := range ch {
		marshalledEvent, err := es.serializer.Serialize(evt)
		if err != nil {
			return err
		}

		es.events = append(es.events, marshalledEvent)
	}

	return nil
}

func (es *inMemory) writeSlice(events []event.Envelope) {
	ch := make(chan event.Envelope)
	go func(events []event.Envelope) {
		defer close(ch)

		for _, e := range events {
			ch <- e
		}
	}(events)

	if err := es.Write(ch); err != nil {
		panic(err)
	}
}
