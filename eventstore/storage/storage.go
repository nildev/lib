package storage

import "bitbucket.org/nildev/lib/eventstore/event"

// EventsReader reads all events for given aggregate and sends them to the specified channel.
// After all events are sent to channel and implementation MUST close the channel.
type EventsReader interface {
	Read(aggregateID string, ch chan<- event.Envelope) error
	ReadAll(ch chan<- event.Envelope) error
}

// EventsWriter writes events from the given channel to the storage.
// Implementations expects that channel will be closed once all events are sent
type EventsWriter interface {
	Write(ch <-chan event.Envelope) error
}

// EventStore is the interface that groups the basic EventsReader and EventsWriter methods.
type EventStore interface {
	EventsReader
	EventsWriter
}
