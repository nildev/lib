package event

import "time"

type (
	// Event data.
	Event interface {
		EventName() string
		AggregateID() string
	}

	// Envelope which combines Event with Metadata.
	Envelope interface {
		Event() Event
		EventName() string
		AggregateID() string
		AggregateVersion() int
		Timestamp() time.Time
		RequestID() string
	}

	// Serializer interface for Envelope serialization
	Serializer interface {
		Serialize(Envelope) (string, error)
	}

	// Unserializer interface for Envelope unserialization
	Unserializer interface {
		Unserialize(string) (Envelope, error)
	}

	// SerializeUnserializer interface
	SerializeUnserializer interface {
		Serializer
		Unserializer
	}

	// DataEvent type used for arbitrary events
	DataEvent struct {
		eventName   string
		aggregateID string
		data        string
	}
)

// EventName impl
func (de *DataEvent) EventName() string {
	return de.eventName
}

// AggregateID impl
func (de *DataEvent) AggregateID() string {
	return de.aggregateID
}

// Data of event
func (de *DataEvent) Data() string {
	return de.data
}

// NewEventEnvelope creates a new instance of Event Envelope
func NewEventEnvelope(
	event Event,
	aggregateVersion int,
	timestamp time.Time,
	requestID string,
) Envelope {
	return &envelope{
		event,
		aggregateVersion,
		timestamp,
		requestID,
	}
}

// NewArbitraryEventEnvelope creates a new instance of Event Envelope
func NewArbitraryEventEnvelope(
	event Event,
	aggregateVersion int,
	timestamp time.Time,
	requestID string,
) Envelope {
	return &arbitraryEnvelope{
		event,
		aggregateVersion,
		timestamp,
		requestID,
	}
}

// Private stuff: implementation of Event interface

type (
	envelope struct {
		event            Event
		aggregateVersion int
		timestamp        time.Time
		requestID        string
	}

	arbitraryEnvelope struct {
		event            Event
		aggregateVersion int
		timestamp        time.Time
		requestID        string
	}
)

func (e *envelope) Event() Event {
	return e.event
}

func (e *envelope) EventName() string {
	return e.event.EventName()
}

func (e *envelope) AggregateID() string {
	return e.event.AggregateID()
}

func (e *envelope) AggregateVersion() int {
	return e.aggregateVersion
}

func (e *envelope) Timestamp() time.Time {
	return e.timestamp
}

func (e *envelope) RequestID() string {
	return e.requestID
}

// arbitraryEnvelope impl of Envelope
func (e *arbitraryEnvelope) Event() Event {
	return e.event
}

func (e *arbitraryEnvelope) EventName() string {
	return e.event.EventName()
}

func (e *arbitraryEnvelope) AggregateID() string {
	return e.event.AggregateID()
}

func (e *arbitraryEnvelope) AggregateVersion() int {
	return e.aggregateVersion
}

func (e *arbitraryEnvelope) Timestamp() time.Time {
	return e.timestamp
}

func (e *arbitraryEnvelope) RequestID() string {
	return e.requestID
}
