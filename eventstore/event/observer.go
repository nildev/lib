package event

import (
	"sync"

	"bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"

	"fmt"
	"strings"

	"bitbucket.org/nildev/lib/log"
	"bitbucket.org/nildev/lib/utils"
)

type (
	// Name type is the name of Event
	Name string

	// HandlerFunc type
	HandlerFunc func(Envelope) error

	// HandlersMap handlers map
	HandlersMap map[Name][]HandlerFunc

	// HandlerMapper type
	HandlerMapper interface {
		GetMap() HandlersMap
	}

	internalHandlersMap map[Name]map[string][]HandlerFunc

	// Observer type
	Observer struct {
		handlers internalHandlersMap
	}
)

// HandleEvent event by passing to specific handler
// if no handler is registered event will be by passed
func (o *Observer) HandleEvent(evt Envelope) error {
	if evt == nil {
		return errors.Trace(errors.New("Event not converted to object, recheck JSON structure"))
	}

	var wg sync.WaitGroup
	if hAll, ok := o.handlers[Name(evt.Event().EventName())]; ok { // map[string][]handlers
		for name, handlers := range hAll { // []handlers
			for _, h := range handlers {
				wg.Add(1)
				// Execute handlers concurrently
				go func(h HandlerFunc, evt Envelope, name string) {
					defer wg.Done()
					defer func() {
						if err := utils.PanicRecover(); err != nil {
							log.Error(err)
						}
					}()
					o.executeHandler(h, evt, name)
				}(h, evt, name)
			}

		}
	}
	wg.Wait()

	return nil
}

func (o *Observer) executeHandler(h HandlerFunc, evt Envelope, name string) {
	if err := h(evt); err != nil {
		log.Errorf("Unhandled event: %s. Error: %v. Handler: %s, Handlers %s", o.eventDescription(evt), err.Error(), name, o.handlerNames())
	}
}

// Publish events
func (o *Observer) Publish(ch <-chan Envelope) error {
	for e := range ch {
		if err := o.HandleEvent(e); err != nil {
			return errors.Trace(err)
		}
	}

	return nil
}

// eventDescription generates string representation of event. So there would be less pointer numbers or fields without values
func (o *Observer) eventDescription(e Envelope) string {
	return fmt.Sprintf("{EventName: %s, AggregateID: %s, RequestID: %s, Event: %+v}", e.EventName(), e.AggregateID(), e.RequestID(), e.Event())
}

// Generates list of registered event handlers
func (o *Observer) handlerNames() string {
	var names = make([]string, 0, len(o.handlers))
	for eventName := range o.handlers {
		names = append(names, string(eventName))
	}
	return strings.Join(names, ", ")
}

// NewObserver constructor
func NewObserver(ehm map[string]HandlerMapper) *Observer {
	eventHandlers := internalHandlersMap{}

	for k, v := range ehm {
		for evtName, hls := range v.GetMap() {
			if eventHandlers[evtName] == nil {
				eventHandlers[evtName] = make(map[string][]HandlerFunc)
			}

			eventHandlers[evtName][k] = hls
		}
	}

	return &Observer{
		handlers: eventHandlers,
	}
}
