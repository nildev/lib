package event

import (
	"bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"
	"encoding/json"
	"reflect"
	"time"
)

// RegisterUnmarshaller registers the type which should be used when unmarshalling event.
// You can pass nil pointer to this method if you don't want to allocate memory,
// but Name method must not fail if called on nil.
func RegisterUnmarshaller(unmarshaller Event) {
	t := reflect.TypeOf(unmarshaller)

	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	unmarshallers[unmarshaller.EventName()] = t
}

// Serialize creates JSON representation of Event Envelope
func Serialize(e Envelope) (string, error) {
	b, err := json.Marshal(e)
	if err != nil {
		return "", errors.Trace(err)
	}

	return string(b), nil
}

// Deserialize restores Event Envelope object from JSON representation
func Deserialize(s string) (Envelope, error) {
	e := &envelope{}
	err := json.Unmarshal([]byte(s), e)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return e, nil
}

func (e *envelope) MarshalJSON() ([]byte, error) {
	se := &serializedEnvelope{
		e.Event(),
		serializedMetadata{
			e.EventName(),
			1,
			e.AggregateID(),
			e.AggregateVersion(),
			e.Timestamp().Format(time.RFC3339),
			e.RequestID(),
		},
	}

	b, err := json.Marshal(se)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return b, nil
}

func (e *envelope) UnmarshalJSON(b []byte) error {
	ue := &unserializedEnvelope{}
	err := json.Unmarshal(b, ue)
	if err != nil {
		return err
	}

	et, ok := unmarshallers[ue.Metadata.EventName]
	if !ok {
		return errors.Trace(errors.New("Can't find unmarshaller for event: " + ue.Metadata.EventName))
	}

	d := reflect.New(et).Interface()
	err = json.Unmarshal(ue.Data, d)
	if err != nil {
		return err
	}

	var evt Event
	evt, ok = d.(Event)
	if !ok {
		return errors.Trace(errors.New("Unable to cast event json " + ue.Metadata.EventName))
	}

	if evt.AggregateID() != ue.Metadata.AggregateID {
		return errors.Trace(errors.New("Aggregate ID from Event does not match Aggregate ID from metadata"))
	}

	t, err := time.Parse(time.RFC3339, ue.Metadata.Timestamp)
	if err != nil {
		return err
	}

	e.event = evt
	e.aggregateVersion = ue.Metadata.AggregateVersion
	e.timestamp = t
	e.requestID = ue.Metadata.RequestID

	return nil
}

// Private stuff

var unmarshallers = make(map[string]reflect.Type)

type (
	serializedMetadata struct {
		EventName        string `json:"eventName"`
		EventVersion     int    `json:"eventVersion"`
		AggregateID      string `json:"aggregateId"`
		AggregateVersion int    `json:"aggregateVersion"`
		Timestamp        string `json:"timestamp"`
		RequestID        string `json:"command"`
	}

	serializedEnvelope struct {
		Data     interface{}        `json:"data"`
		Metadata serializedMetadata `json:"metadata"`
	}

	unserializedEnvelope struct {
		Data     json.RawMessage    `json:"data"`
		Metadata serializedMetadata `json:"metadata"`
	}
)
