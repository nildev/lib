package event

import "bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"

type (
	// EnvelopePipe type
	EnvelopePipe interface {
		Process(in chan Envelope, errCh chan error) chan Envelope
	}

	// EnvelopePipeline type
	EnvelopePipeline struct {
		start chan Envelope
		end   chan Envelope
		errCh chan error
	}
)

// Add item
func (p *EnvelopePipeline) Add(item Envelope) {
	p.start <- item
}

// Read item
func (p *EnvelopePipeline) Read(handler func(Envelope)) {
	for i := range p.end {
		handler(i)
	}
}

// Close channel
func (p *EnvelopePipeline) Close() {
	// indicate that we take no more items and
	// client should wait for end chan to be closed
	close(p.start)
}

// Wait for result of pipeline
func (p *EnvelopePipeline) Wait() error {
	for {
		select {
		case _, more := <-p.end:
			if !more {
				return nil
			}
		case err := <-p.errCh:
			return errors.Trace(err)
		default:
		}

	}
}

// NewEventEnvelopePipeline constructor
func NewEventEnvelopePipeline(stages ...EnvelopePipe) EnvelopePipeline {
	errCh := make(chan error)
	beginning := make(chan Envelope)

	var nextChan chan Envelope
	for _, pipe := range stages {
		if nextChan == nil {
			nextChan = pipe.Process(beginning, errCh)
		} else {
			nextChan = pipe.Process(nextChan, errCh)
		}
	}

	return EnvelopePipeline{start: beginning, end: nextChan, errCh: errCh}
}
