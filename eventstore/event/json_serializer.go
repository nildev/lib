package event

import (
	"encoding/json"

	"bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"
	"time"
)

// NewJSONUnserializer Creates Unserializer
func NewJSONUnserializer() Unserializer {
	return &jsonUnserializer{}
}

// NewArbitraryJSONUnserializer Creates Unserializer
func NewArbitraryJSONUnserializer() Unserializer {
	return &jsonArbitraryUnserializer{}
}

// NewJSONSerializer Creates instance of serializer
func NewJSONSerializer() Serializer {
	return &jsonSerializer{}
}

// NewJSONSerializerUnserializer Create serializer capable of serializing and unserializing
func NewJSONSerializerUnserializer(serializer Serializer, unserializer Unserializer) SerializeUnserializer {
	return &jsonSerializerUnserializer{serializer, unserializer}
}

type jsonUnserializer struct{}
type jsonArbitraryUnserializer struct{}
type jsonSerializer struct{}

type jsonSerializerUnserializer struct {
	Serializer
	Unserializer
}

// Serialize takes command and returns JSON serialized command
func (s *jsonSerializer) Serialize(evt Envelope) (string, error) {
	b, err := json.Marshal(evt)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

// Unserialize takes JSON input and creates Envelop from it
func (s *jsonUnserializer) Unserialize(raw string) (Envelope, error) {
	e := &envelope{}
	err := json.Unmarshal([]byte(raw), e)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return e, nil
}

// Unserialize takes JSON input and creates Envelop from it
func (s *jsonArbitraryUnserializer) Unserialize(raw string) (Envelope, error) {
	e := &arbitraryEnvelope{}
	err := json.Unmarshal([]byte(raw), e)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return e, nil
}

func (e *arbitraryEnvelope) MarshalJSON() ([]byte, error) {
	se := &serializedEnvelope{
		e.Event(),
		serializedMetadata{
			e.EventName(),
			1,
			e.AggregateID(),
			e.AggregateVersion(),
			e.Timestamp().Format(time.RFC3339),
			e.RequestID(),
		},
	}

	b, err := json.Marshal(se)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return b, nil
}

func (e *arbitraryEnvelope) UnmarshalJSON(b []byte) error {
	ue := &unserializedEnvelope{}
	err := json.Unmarshal(b, ue)
	if err != nil {
		return errors.Trace(err)
	}

	d := &DataEvent{
		aggregateID: ue.Metadata.AggregateID,
		eventName:   ue.Metadata.EventName,
		data:        string(ue.Data),
	}

	t, err := time.Parse(time.RFC3339, ue.Metadata.Timestamp)
	if err != nil {
		return errors.Trace(err)
	}

	e.event = Event(d)
	e.aggregateVersion = ue.Metadata.AggregateVersion
	e.timestamp = t
	e.requestID = ue.Metadata.RequestID

	return nil
}
