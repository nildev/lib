package event

// NewInMemoryPublisher creates new publisher for tests.
func NewInMemoryPublisher(ch chan<- Envelope) Publisher {
	return &inMemoryPublisher{ch}
}

type inMemoryPublisher struct {
	events chan<- Envelope
}

// Process implements pipeline interface
func (p *inMemoryPublisher) Process(in chan Envelope, errCh chan error) chan Envelope {
	buffer := make(chan Envelope, 100)
	out := make(chan Envelope)
	// We write all events first and only then we start writing to outbound channel
	go func() {
		for evt := range in {
			nc := make(chan Envelope, 1)
			nc <- evt
			close(nc)
			err := p.Publish(nc)
			if err != nil {
				errCh <- err
				// we stop processing events
				break
			}
			// pass forward
			buffer <- evt
		}

		close(buffer)
		for evt := range buffer {
			out <- evt
		}
		close(out)
	}()

	return out
}

func (p *inMemoryPublisher) Publish(ch <-chan Envelope) error {
	for e := range ch {
		// Simulate that event wen't through some 3rd party system
		str, err := Serialize(e)
		if err != nil {
			return err
		}

		e2, err := Deserialize(str)
		if err != nil {
			return err
		}

		p.events <- e2
	}

	return nil
}
