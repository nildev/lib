package event

import (
	"sync"

	"bitbucket.org/nildev/lib/utils"
	"bitbucket.org/nildev/lib/log"
)

// Publisher takes events from the channel and sends them to the Event Queue.
// Channel should be closed when all events are sent.
type Publisher interface {
	Publish(<-chan Envelope) error
}

// AggregatedPublisher is structure behind AggregatedPublisher
type AggregatedPublisher struct {
	syncPublishers  []Publisher
	asyncPublishers []Publisher
}

// NewAggregatedPublisher creates publisher with forwards events to publishers
func NewAggregatedPublisher(syncPublishers []Publisher, asyncPublishers []Publisher) *AggregatedPublisher {
	return &AggregatedPublisher{syncPublishers: syncPublishers, asyncPublishers: asyncPublishers}
}

// Process implements pipeline interface
func (p *AggregatedPublisher) Process(in chan Envelope, errCh chan error) chan Envelope {
	buffer := make(chan Envelope, 100)
	out := make(chan Envelope)
	// We write all events first and only then we start writing to outbound channel
	go func() {
		for evt := range in {
			nc := make(chan Envelope, 1)
			nc <- evt
			close(nc)
			err := p.Publish(nc)
			if err != nil {
				errCh <- err
				// we stop processing events
				break
			}
			// pass forward
			buffer <- evt
		}

		close(buffer)
		for evt := range buffer {
			out <- evt
		}
		close(out)
	}()

	return out
}

// Publish publishes each Envelope to publishers
func (p *AggregatedPublisher) Publish(c <-chan Envelope) error {
	var wg sync.WaitGroup

	errCh := make(chan error)
	for event := range c {
		for _, publisher := range p.syncPublishers {
			wg.Add(1)
			go func(publisher Publisher, event Envelope, errCh chan<- error) {
				defer wg.Done()
				defer func() {
					if err := utils.PanicRecover(); err != nil {
						log.Error(err)
					}
				}()
				p.publishEvent(publisher, event, errCh)

			}(publisher, event, errCh)
		}

		for _, publisher := range p.asyncPublishers {
			go func(publisher Publisher, event Envelope, errCh chan<- error) {
				defer func() {
					if err := utils.PanicRecover(); err != nil {
						log.Error(err)
					}
				}()
				p.publishEvent(publisher, event, errCh)

			}(publisher, event, errCh)
		}
	}

	wg.Wait()

	return nil
}

func (p *AggregatedPublisher) publishEvent(publisher Publisher, event Envelope, errCh chan<- error) {

	nc := make(chan Envelope, 1)
	nc <- event

	close(nc)
	err := publisher.Publish(nc)
	if err != nil {
		log.Error(err)
	}
}
