package event

import (
	"time"
)

// Reader that wraps events reading methods.
type Reader interface {
	Read(chan<- Envelope)
}

// Provider is an entity which will be generating events
// and Read method would use given chan to stream generated events to the reader.
type Provider interface {
	Reader

	AppendNewEvent(event Event) Envelope
	SetCurrentVersion(int)
	SetRequestID(string)
}

// ProviderImplementation implements Provider interface and can be included into structs to provide
// default implementation of Provider interface.
type ProviderImplementation struct {
	events         []Envelope
	currentVersion int
	requestID      string
}

func (p *ProviderImplementation) Read(ch chan<- Envelope) {
	defer close(ch)

	for _, e := range p.events {
		ch <- e
	}
}

// AppendNewEvent adds an Event to the Provider
func (p *ProviderImplementation) AppendNewEvent(event Event) Envelope {
	e := NewEventEnvelope(
		event,
		p.currentVersion+1,
		time.Now(),
		p.requestID,
	)

	p.events = append(p.events, e)
	p.currentVersion++

	return e
}

// SetCurrentVersion updates current version.
// This can only be done once and no events should've been appended yet.
func (p *ProviderImplementation) SetCurrentVersion(version int) {
	if p.currentVersion != 0 {
		panic("Current version was already modified either by SetCurrentVersion or by AppendEvent")
	}

	p.currentVersion = version
}

// SetRequestID for currently generated Events.
func (p *ProviderImplementation) SetRequestID(requestID string) {
	p.requestID = requestID
}
