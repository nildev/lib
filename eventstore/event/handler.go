package event

// Handler takes Event instance and updates Query Model
// Channel MUST be closed when all events are sent.
type Handler interface {
	Handle() error
	Consume() error
	Stop() error
}

// Consumer consumes Event
type Consumer interface {
	Consume() error
}
