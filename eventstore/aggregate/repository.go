package aggregate

import (
	"bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"
	"bitbucket.org/nildev/lib/eventstore/event"
	"bitbucket.org/nildev/lib/eventstore/storage"
	"bitbucket.org/nildev/lib/utils"
)

// Repository can load events into Aggregate Root and save newly generated events into Event Store.
type Repository interface {
	Load(Root, string) error
	Save(Root) error
}

// NewRepository creates a new instance of Repository backed by provided Event Store.
func NewRepository(es storage.EventStore, p event.Publisher) Repository {
	return &repository{
		eventStore:     es,
		eventPublisher: p,
	}
}

// Private stuff

type repository struct {
	eventStore     storage.EventStore
	eventPublisher event.Publisher
}

func panicRecover(errCh chan<- error) {
	if err := utils.PanicRecover(); err != nil {
		errCh <- err
	}
}

// Load impl
func (r *repository) Load(ar Root, id string) error {
	ch := make(chan event.Envelope)

	chReadFin := make(chan error)
	chLoadFin := make(chan error)

	// Start reading events from event store
	go func() {
		defer close(chReadFin)
		defer panicRecover(chReadFin)

		err := r.eventStore.Read(id, ch)
		if err != nil {
			chReadFin <- err
		}
	}()

	// Load events concurrently for aggregate and it's child entities
	go func() {
		defer close(chLoadFin)
		defer panicRecover(chLoadFin)

		err := LoadEvents(ar, ch)
		if err != nil {
			chLoadFin <- err
		}
	}()

	if err := <-chReadFin; err != nil {
		return errors.Trace(err)
	}

	if err := <-chLoadFin; err != nil {
		return errors.Trace(err)
	}

	return nil
}

// Save impl
func (r *repository) Save(ar Root) error {
	// resetPipe is required to reset new pipeline after every Save request
	eventEnvelopePipeline := event.NewEventEnvelopePipeline(
		r.eventStore.(event.EnvelopePipe),
		r.eventPublisher.(event.EnvelopePipe),
	)

	chRead := make(chan event.Envelope)
	// Read events from aggregate
	go ar.Read(chRead)

	// Add events to pipeline for processing
	for e := range chRead {
		eventEnvelopePipeline.Add(e)
	}

	eventEnvelopePipeline.Close()

	// wait for results
	if err := eventEnvelopePipeline.Wait(); err != nil {
		return errors.Trace(err)
	}

	return nil
}
