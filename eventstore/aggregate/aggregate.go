package aggregate

import (
	"reflect"

	"bitbucket.org/nildev/lib/Godeps/_workspace/src/github.com/juju/errors"
	"bitbucket.org/nildev/lib/eventstore/event"
)

// Entity is something that has a state and life cycle.
// Identified domain entities should embed this struct.
type Entity interface {
	EntityID() string
}

// Root is an entity which aggregates child entities.
// All external communication to the Entities inside the Aggregate happen through the Root.
// Aggregate Root also acts as a Event Provider
type Root interface {
	Entity
	event.Provider
}

// ChildEntity is an entity which lies inside an Aggregate.
// It doesn't manage it's own events and does not respond to external communication.
type ChildEntity interface {
	Entity

	AggregateRoot() Root
	SetAggregateRoot(Root)
}

// RootBase provides base implementation for any struct which can act as an Aggregate Root.
type RootBase struct {
	event.ProviderImplementation
}

// ChildEntityBase provides base implementation for any struct which can act as a Child Entity.
type ChildEntityBase struct {
	aggregateRoot Root
}

// AggregateRoot return an Aggregate Root instance associated with given Child Entity.
func (ce *ChildEntityBase) AggregateRoot() Root {
	return ce.aggregateRoot
}

// SetAggregateRoot associates Child Entity with given Aggregate Root.
// Can only be performed once.
func (ce *ChildEntityBase) SetAggregateRoot(ar Root) {
	if ce.aggregateRoot != nil {
		panic("Aggregate Root is already set")
	}

	ce.aggregateRoot = ar
}

// HandleEvent creates new Event and applies it to Aggregate (adds to storage, calls handlers).
// Should be called from behaviors inside Entities.
func HandleEvent(entity Entity, event event.Event) {
	ar := getEntityRoot(entity)

	// Pass event to entity
	e := ar.AppendNewEvent(event)

	// Apply event
	if err := applyEvent(e, ar); err != nil {
		panic(err)
	}
}

// LoadEvents reads Events from the given channel and applies them to an Aggregate Root.
func LoadEvents(ar Root, events <-chan event.Envelope) error {
	counter := 0
	for e := range events {
		if (e.AggregateVersion() - counter) != 1 {
			return errors.Trace(errors.New("Events are not in order"))
		}
		counter++

		if err := applyEvent(e, ar); err != nil {
			return errors.Trace(err)
		}
	}

	ar.SetCurrentVersion(counter)
	return nil
}

// Private stuff

// Entity implementation

func applyEvent(e event.Envelope, ar Root) error {
	ev := e.Event()

	// change state by calling event handler of passed entity with event as param
	methodName := "On" + ev.EventName()

	var ptr reflect.Value
	var value reflect.Value
	var finalMethod reflect.Value

	value = reflect.ValueOf(ar)

	// if we start with a pointer, we need to get value pointed to
	// if we start with a value, we need to get a pointer to that value
	if value.Type().Kind() == reflect.Ptr {
		ptr = value
		value = ptr.Elem()
	} else {
		ptr = reflect.New(reflect.TypeOf(ar))
		temp := ptr.Elem()
		temp.Set(value)
	}

	// check for method on value
	method := value.MethodByName(methodName)
	if method.IsValid() {
		finalMethod = method
	}
	// check for method on pointer
	method = ptr.MethodByName(methodName)
	if method.IsValid() {
		finalMethod = method
	}

	if finalMethod.IsValid() {
		in := []reflect.Value{reflect.ValueOf(ev)}
		finalMethod.Call(in)
		return nil
	}

	return errors.Trace(errors.New("Event Handler not found: " + methodName))
}

func getEntityRoot(e Entity) Root {
	switch ent := e.(type) {
	case Root:
		return ent
	case ChildEntity:
		return ent.AggregateRoot()
	default:
		panic("Can't detect entity type")
	}
}
